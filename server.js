const http = require('http')

const handleRequest = (request, response) => {
    console.log('Received request for URL: ' + request.url)
    response.writeHead(200)
    response.end('Server v1')
}
const www = http.createServer(handleRequest)
www.listen(6500)